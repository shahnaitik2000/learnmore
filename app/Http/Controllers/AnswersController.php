<?php

namespace App\Http\Controllers;

use App\Http\Requests\CreateAnswerRequest;
use App\Http\Requests\UpdateAnswerRequest;
use App\Models\Answer;
use App\Models\Question;
use App\Notifications\NewAnswerAdded;
use Illuminate\Http\Request;

class AnswersController extends Controller
{
    public function store(Question $question, CreateAnswerRequest $request)
    {
        if($request->file('image'))
        {
            $image = $request->file('image')->store('images/posts');
        }
        else
        {
            $image = null;
        }
        $question->answers()->create(['body' => $request->body, 'user_id' => auth()->id(), 'image' => $image]);
        $question->owner->notify(new NewAnswerAdded($question));
        session()->flash('success', 'Your answered is Added!');
        return redirect($question->url);
    }

    public function edit(Question $question, Answer $answer)
    {
        $this->authorize('update', $answer);
        return  view('answers.edit', compact(['question','answer']));
    }

    public function update(UpdateAnswerRequest $request, Question $question, Answer $answer)
    {
        $this->authorize('update',$answer);
        $dataSet = ['body' => $request->body ];
        if($request->image)
        {
            $answer->deleteImg();
            $image = $request->file('image')->store('images/posts');
            $dataSet = ['body' => $request->body, 'image' => $image];
        }

        $answer->update($dataSet);
        session()->flash('success', 'Your answered is Edited!');
        return redirect($question->url);
    }

    public function destroy(Question $question, Answer $answer)
    {
        $this->authorize('delete',$answer);
        $answer->deleteImg();
        $answer->delete();
        session()->flash('success', 'Answered is Deleted!');
        return redirect($question->url);
    }

    public function bestAnswer(Answer $answer)
    {
        $this->authorize('markAsBest',$answer);
        $answer->question->markBestAnswer($answer);
        return redirect()->back();
    }
}
