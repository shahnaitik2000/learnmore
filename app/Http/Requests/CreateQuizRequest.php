<?php

namespace App\Http\Requests;

use Carbon\Carbon;
use Illuminate\Foundation\Http\FormRequest;

class CreateQuizRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return auth()->check();
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $today = Carbon::yesterday();
        return [
            'title' => 'required',
            'outoff_marks' => 'integer|required|min:0',
            'date' => 'required|date|after:'.$today,
            'duration' => 'required|integer|min:1|max:240',
            'time' => 'required|date_format:H:i',
            'results' => 'required|in:YES,NO'
        ];
    }
}
