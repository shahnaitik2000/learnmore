<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Storage;

class UserAssignment extends Model
{
    use HasFactory;
    protected $guarded = [''];
    protected $table = 'user_assignment';

    public function assignment()
    {
        return $this->belongsTo(Assignment::class);
    }

    public function assignedTo()
    {
        return $this->belongsTo(User::class, 'user_id');
    }

    public function getDocPathAttribute()
    {
        return 'storage/'.$this->doc;
    }

    public function deleteDoc()
    {
        Storage::delete($this->doc);
    }
}
