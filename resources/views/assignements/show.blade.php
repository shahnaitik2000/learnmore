@extends('layouts.app')
@section('page-level-styles')

@endsection
@section('content')
<div class="container">
    <a class="mb-3 btn btn-primary" href="{{ route('assignments.index') }}">
        Back
    </a>
    <div class="row justify-content-center">
        <div class="col-md-12">

            <div class="card">
                <div class="card-header">
                    <h1 class="pull-left">{{ $assignment->title }} &nbsp;&nbsp;&nbsp;ID:{{ $assignment->id }}</h1>
                    <a href="{{ route('assignments.edit',$assignment->id) }}" class="btn btn-warning pull-right">Edit</a>
                    <form action="{{ route('assignments.destroy',$assignment->id) }}" method="POST" class="mr-3 pull-right">
                        @csrf
                        @method('DELETE')
                        <button type="submit" class="btn btn-danger" onclick="return confirm('Are you sure you want to delete it?')">Delete</button>
                    </form>
                </div>
                <div class="card-body">
                    {!! $assignment->description !!}

                    <p>Ref Doc: <a href="{{ asset($assignment->ref_path) }}">View file</a></p>
                </div>
                <div class="card-footer">
                    <div class="d-flex justify-content-between">
                        <div class="d-flex">

                        </div>
                        <div class="d-flex flex-column">
                            <div class="text-muted mb-2 text-right">
                                <p>Due Date - {{ $assignment->due_date }}</p>
                            </div>
                            <div class="text-muted mb-2 text-right">
                                <p>Due Time - {{ $assignment->due_time }}</p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <div class="card mt-4">
                <div class="card-header">
                    <h3 class="pull-left">Student List</h3>
                    <form action="{{ route('notifystudents', $assignment) }}" method="POST">
                        @csrf
                        <button type="submit" class="btn btn-danger pull-right ml-2">Notify Students</button>
                    </form>
                    <a href="" id="excel_button" class="btn btn-success pull-right">Download Excel</a>
                </div>
                <div class="card-body">

                    <table class="table table-hover" id="student_table">
                        <thead>
                            <tr>
                              <th scope="col">#</th>
                              <th scope="col">Name</th>
                              <th scope="col">Status</th>
                              <th scope="col">Submission Date</th>
                              <th scope="col">Submission Time</th>
                              <th scope="col">Marks</th>
                            </tr>
                          </thead>

                          @foreach ($userassignments as $userassignment)
                          <tr>
                            <th scope="row">{{ $loop->index+1 }}</th>

                                <td>{{ $userassignment->assignedTo->name }}</td>

                                @if(!$userassignment->submission_date)
                                    <td>Not Submitted</td>
                                @else
                                    <td><a href="{{ route('teachers.assignment.show', $userassignment->id) }}" class="">Submitted</a></td>
                                @endif

                                <td>{{ $userassignment->submission_date }}</td>
                                <td>{{ $userassignment->submission_time }}</td>

                                @if ($userassignment->alloted_marks)
                                    <td>{{ $userassignment->alloted_marks }} / {{ $userassignment->assignment->total_marks }}</td>
                                @else
                                    <td>Marks not alloted</td>
                                @endif

                          </tr>
                            @endforeach
                    </table>


                </div>
                <div class="card-footer">
                    <div class="d-flex justify-content-between">
                        <div class="d-flex">

                        </div>
                        <div class="d-flex flex-column">

                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

</div>
@endsection
@section('page-level-scripts')
<script type="text/javascript" src="https://unpkg.com/xlsx@0.15.1/dist/xlsx.full.min.js"></script>
<script>
    function html_table_to_excel(type)
    {
        var data = document.getElementById('student_table');

        var file = XLSX.utils.table_to_book(data, {sheet: "sheet1"});

        XLSX.write(file, { bookType: type, bookSST: true, type: 'base64' });

        XLSX.writeFile(file, 'file.' + type);
    }

    const export_button = document.getElementById('excel_button');

    export_button.addEventListener('click', () =>  {
        html_table_to_excel('xlsx');
    });
</script>
@endsection


