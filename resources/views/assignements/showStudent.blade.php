@extends('layouts.app')
@section('page-level-styles')
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/trix/1.3.1/trix.css" integrity="sha512-CWdvnJD7uGtuypLLe5rLU3eUAkbzBR3Bm1SFPEaRfvXXI2v2H5Y0057EMTzNuGGRIznt8+128QIDQ8RqmHbAdg==" crossorigin="anonymous" referrerpolicy="no-referrer" />
<style>
    trix-toolbar .trix-button--icon-attach{
  display: none;
}
</style>
@endsection
@section('content')
<div class="container">
    <a class="mb-3 btn btn-primary" href="{{ route('students.assignment.index') }}">
        Back
    </a>

    <div class="row justify-content-center">
        <div class="col-md-12">

            <div class="card">
                <div class="card-header">
                    <h1 class="pull-left">{{ $userassignment->assignment->title }} &nbsp;&nbsp;&nbsp;ID:{{ $userassignment->assignment->id }}</h1>
                    <form class="pull-right" action="{{ route('students.assignment.withdraw', $userassignment->id) }}" method="POST">
                        @csrf
                        <button type="submit" class="btn btn-danger" onclick="return confirm('Are you sure you want to delete it?')">Delete</button>
                    </form>
                </div>
                <div class="card-body">
                    {!! $userassignment->assignment->description !!}

                    <p>Ref Doc: <a href="{{ asset($userassignment->assignment->ref_path) }}">View file</a></p>
                </div>
                <div class="card-footer">
                    <div class="d-flex justify-content-between">
                        <div class="d-flex">

                        </div>
                        <div class="d-flex flex-column">
                            <div class="text-muted mb-2 text-right">
                                <p>Due Date - {{ $userassignment->assignment->due_date }}</p>
                            </div>
                            <div class="text-muted mb-2 text-right">
                                <p>Due Time - {{ $userassignment->assignment->due_time }}</p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <div class="card mt-5">
                <div class="card-header">

                    @if(!$userassignment->submission_date)
                        <h3 class="pull-left text-danger">Not Submitted</h3>

                    @else
                        <h3 class="pull-left text-success">Submitted</h3>

                        <h5 class="pull-right">Submission date and time: {{ $userassignment->submission_date }} {{ $userassignment->submission_time }}</h5>
                    @endif
                </div>
                <div class="card-body">
                    @if ($errors)
                    <ul>
                        @foreach ($errors->all() as $message)
                            <li>{{ $message }}</li>
                        @endforeach
                    </ul>
                    @endif

                    @if(!$userassignment->doc)
                        <form action="{{ route('students.assignment.update', $userassignment->id) }}" method="POST" enctype="multipart/form-data">
                            @csrf
                            @method('PUT')

                                <label for="file1" class="form-label">Assignment Doc</label>
                                <input id="file1" type="file" name="doc" class="form-control">
                                <label for="text1" class="mt-3 form-label">Assignment Doc</label>
                                <input id="text1" type="text" name="textOrfeedback" class="form-control" placeholder="Write it here">

                                <button class="mt-5 btn btn-primary" type="submit">Submit</button>

                        </form>
                    @else

                        <p>Submitted Doc: <a href="{{ asset($userassignment->doc_path) }}">Click Here</a></p>
                        <p>If you wish to edit submission that date and time of submission would automaticaaly set to presents</p>

                        @if ($userassignment->alloted_marks)
                            <p>Marks alloted : {{ $userassignment->alloted_marks }} / {{ $userassignment->assignment->total_marks }}</p>
                        @else
                        <form class="mt-5" action="{{ route('students.assignment.update', $userassignment->id) }}" method="POST" enctype="multipart/form-data">
                            @csrf
                            @method('PUT')

                                <label for="file2" class="form-label">Assignment Doc</label>
                                <input id="file2" type="file" name="doc" class="form-control">
                                <label for="text2" class="mt-3 form-label">Assignment text</label>
                                <input id="text2" type="text" name="textOrfeedback" class="form-control" placeholder="Write it here" value="{{ old('textOrfeedback', $userassignment->textOrfeedback) }}">

                                <button class="mt-5 btn btn-warning" type="submit">Edit Submission</button>

                        </form>
                        @endif
                    @endif
                </div>

            </div>

            {{-- <div class="card">
                <div class="card-header"><h1>{{ $students->name }}</h1></div>
                <div class="card-body">
                    {!! $assignment->description !!}
                </div>
                <div class="card-footer">
                    <div class="d-flex justify-content-between">
                        <div class="d-flex">

                        </div>
                        <div class="d-flex flex-column">
                            <div class="text-muted mb-2 text-right">
                                <p>Due Date - {{ $assignment->due_date }}</p>
                            </div>

                        </div>
                    </div>
                </div>
            </div> --}}
        </div>
    </div>

</div>
@endsection
@section('page-level-scripts')
<script src="https://cdnjs.cloudflare.com/ajax/libs/trix/1.3.1/trix.js" integrity="sha512-/1nVu72YEESEbcmhE/EvjH/RxTg62EKvYWLG3NdeZibTCuEtW5M4z3aypcvsoZw03FAopi94y04GhuqRU9p+CQ==" crossorigin="anonymous" referrerpolicy="no-referrer"></script>
<script>
    function clearAll()
    {
        var output = document.getElementById("body1");
        output.value = "";
    }


    function runSpeechRecognition() {

		        var output = document.getElementById("body1");
		        // get action element reference
		        var action = document.getElementById("action");
                // new speech recognition object
                var SpeechRecognition = SpeechRecognition || webkitSpeechRecognition;
                var recognition = new SpeechRecognition();

                // This runs when the speech recognition service starts
                recognition.onstart = function() {
                    action.innerHTML = "<small>listening, please speak...</small>";
                };

                recognition.onspeechend = function() {
                    action.innerHTML = "<small>stopped listening, hope you are done...</small>";
                    recognition.stop();
                }

                // This runs when the speech recognition service returns result
                recognition.onresult = function(event) {
                    var transcript = event.results[0][0].transcript;
                    // console.log(transcript);
                    var original_val = output.value;
                    output.value = output.value + " " + transcript;
                    output.classList.remove("hide");
                };

                 // start recognition
                 recognition.start();
	        }
</script>
@endsection


