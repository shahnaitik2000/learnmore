@extends('layouts.app')
@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-12">
            <div class="card">
                <div class="card-header"><h2>Ask A Question!</h2></div>
                <div class="card-body">
                    <form action="{{ route('questions.store') }}" method="POST" enctype="multipart/form-data">
                        @csrf
                            <div class="form-group">
                                <label for="title">Title</label>
                                <input type="text"
                                       id="title"
                                       name="title"
                                       value="{{ old('title') }}"
                                       class="form-control {{ $errors->has('title') ? 'is-invalid' : ''}}">
                                @error('title')
                                    <div class="text-danger">{{ $message }}</div>
                                @enderror
                            </div>

                            <div class="form-group">
                                <label for="">Enter a Question</label>
                                <input type="hidden" id="body" name="body" value="{{ old('body') }}">
                                <trix-editor input="body" class="form-control {{ $errors->has('body') ? 'is-invalid' : ''}}" id="body1"></trix-editor>
                                @error('body')
                                    <div class="text-danger">{{ $message }}</div>
                                @enderror
                            </div>
                            <div class="form-group">
                                <button class="btn btn-primary" onclick="runSpeechRecognition()" type="button">Speech to text</button>
                                <button class="btn btn-primary" onclick="clearAll()" type="button">Clear All</button>
                                <span id="action"></span>
                            </div>

                            <div class="form-group">
                                <label for="teachers">Ask To Specific Teacher(s) ?</label>
                                <select name="teachers[]" id="teachers" class="form-control select2" multiple>
                                    <option></option>
                                    @foreach ($teachers as $teacher)
                                       <option value="{{ $teacher->id }}"
                                        {{ (old('teachers')&&(in_array($teacher->id, old('teachers'))) ?? 'selected') }}
                                        >{{$teacher->name}}</option>

                                    @endforeach
                                </select>
                                @error('teachers')
                                  <small id="emailHelp" class="form-text text-danger">{{ $message }}</small>
                                @enderror
                              </div>

                            <div class="form-group">
                                <button class="btn btn-success" type="submit">Submit</button>
                            </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
@section('page-level-styles')
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/trix/1.3.1/trix.css" integrity="sha512-CWdvnJD7uGtuypLLe5rLU3eUAkbzBR3Bm1SFPEaRfvXXI2v2H5Y0057EMTzNuGGRIznt8+128QIDQ8RqmHbAdg==" crossorigin="anonymous" referrerpolicy="no-referrer" />
<link href="https://cdn.jsdelivr.net/npm/select2@4.1.0-rc.0/dist/css/select2.min.css" rel="stylesheet" />
<style>
    trix-toolbar .trix-button--icon-attach {
  display: none;
}
</style>
@endsection
@section('page-level-scripts')
<script src="https://cdnjs.cloudflare.com/ajax/libs/trix/1.3.1/trix.js" integrity="sha512-/1nVu72YEESEbcmhE/EvjH/RxTg62EKvYWLG3NdeZibTCuEtW5M4z3aypcvsoZw03FAopi94y04GhuqRU9p+CQ==" crossorigin="anonymous" referrerpolicy="no-referrer"></script>
<script src="https://cdn.jsdelivr.net/npm/select2@4.1.0-rc.0/dist/js/select2.min.js"></script>
<script>
    $('.select2').select2({
        placeholder: 'Select a Teacher',
        allowClear: true
    });

    function clearAll()
    {
        var output = document.getElementById("body1");
        output.value = "";
    }


    function runSpeechRecognition() {

		        var output = document.getElementById("body1");
		        // get action element reference
		        var action = document.getElementById("action");
                // new speech recognition object
                var SpeechRecognition = SpeechRecognition || webkitSpeechRecognition;
                var recognition = new SpeechRecognition();

                // This runs when the speech recognition service starts
                recognition.onstart = function() {
                    action.innerHTML = "<small>listening, please speak...</small>";
                };

                recognition.onspeechend = function() {
                    action.innerHTML = "<small>stopped listening, hope you are done...</small>";
                    recognition.stop();
                }

                // This runs when the speech recognition service returns result
                recognition.onresult = function(event) {
                    var transcript = event.results[0][0].transcript;
                    // console.log(transcript);
                    var original_val = output.value;
                    output.value = output.value + " " + transcript;
                    output.classList.remove("hide");
                };

                 // start recognition
                 recognition.start();
	        }

</script>
@endsection
