@extends('layouts.app')
@section('content')
<a class="btn btn-primary" href="{{ route('quiz.teacher.home') }}">Back to Home</a>
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-12">
            <div class="card">
                <div class="card-header"><h2>Edit Question!</h2></div>
                <div class="card-body">
                    <form action="{{ route('quiz.teacher.question.update', $quizQuestion->id) }}" method="POST" enctype="multipart/form-data">
                        @csrf
                        @method('PUT')
                            <div class="form-group">
                                <label for="question">Question</label>
                                <input type="text"
                                       id="question"
                                       name="question"
                                       value="{{ old('question', $quizQuestion->question) }}"
                                       class="form-control {{ $errors->has('question') ? 'is-invalid' : ''}}">
                                @error('question')
                                    <div class="text-danger">{{ $message }}</div>
                                @enderror
                            </div>

                            <div class="form-group">
                                <label for="option1">Option 1</label>
                                <input type="text"
                                       id="option1"
                                       name="option1"
                                       value="{{ old('option1', $quizQuestion->option1) }}"
                                       class="form-control {{ $errors->has('option1') ? 'is-invalid' : ''}}"
                                       onchange="changeOption(this.name)">
                                @error('option1')
                                    <div class="text-danger">{{ $message }}</div>
                                @enderror
                            </div>

                            <div class="form-group">
                                <label for="option2">Option 2</label>
                                <input type="text"
                                       id="option2"
                                       name="option2"
                                       value="{{ old('option2',$quizQuestion->option2) }}"
                                       class="form-control {{ $errors->has('option2') ? 'is-invalid' : ''}}"
                                       onchange="changeOption(this.name)">
                                @error('option2')
                                    <div class="text-danger">{{ $message }}</div>
                                @enderror
                            </div>

                            <div class="form-group">
                                <label for="option3">Option 3</label>
                                <input type="text"
                                       id="option3"
                                       name="option3"
                                       value="{{ old('option3', $quizQuestion->option3) }}"
                                       class="form-control {{ $errors->has('option3') ? 'is-invalid' : ''}}"
                                       onchange="changeOption(this.name)">
                                @error('option3')
                                    <div class="text-danger">{{ $message }}</div>
                                @enderror
                            </div>

                            <div class="form-group">
                                <label for="option4">Option 4</label>
                                <input type="text"
                                       id="option4"
                                       name="option4"
                                       value="{{ old('option4', $quizQuestion->option4) }}"
                                       class="form-control {{ $errors->has('option4') ? 'is-invalid' : ''}}"
                                       onchange="changeOption(this.name)">
                                @error('option4')
                                    <div class="text-danger">{{ $message }}</div>
                                @enderror
                            </div>


                            <p>Which option is the Answer?</p>
                            <div class="form-check form-check-inline">
                                <input class="form-check-input" type="radio" name="answer" id="option1_radio" value="{{ $quizQuestion->option1 }}"
                                @if ($quizQuestion->option1 == $quizQuestion->answer)
                                    checked
                                @endif
                                >
                                <label class="form-check-label" for="inlineRadio1">Option 1</label>
                            </div>
                              <div class="form-check form-check-inline">
                                <input class="form-check-input" type="radio" name="answer" id="option2_radio" value="{{ $quizQuestion->option2 }}"
                                @if ($quizQuestion->option2 == $quizQuestion->answer)
                                    checked
                                @endif
                                >
                                <label class="form-check-label" for="inlineRadio2">Option 2</label>
                              </div>
                              <div class="form-check form-check-inline">
                                <input class="form-check-input" type="radio" name="answer" id="option3_radio" value="{{ $quizQuestion->option3 }}"
                                @if ($quizQuestion->option3 == $quizQuestion->answer)
                                    checked
                                @endif
                                >
                                <label class="form-check-label" for="inlineRadio2">Option 3</label>
                              </div>
                              <div class="form-check form-check-inline">
                                <input class="form-check-input" type="radio" name="answer" id="option4_radio" value="{{ $quizQuestion->option4 }}"
                                @if ($quizQuestion->option4 == $quizQuestion->answer)
                                    checked
                                @endif
                                >
                                <label class="form-check-label" for="inlineRadio2">Option 4</label>
                              </div>
                              @error('answer')
                                    <div class="text-danger">{{ $message }}</div>
                                @enderror

                            <div class="form-group">
                                <button class="btn btn-success" type="submit">Submit</button>
                            </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
@section('page-level-styles')

@endsection
@section('page-level-scripts')
 <script>
    function changeOption(ele)
    {
        let inputtext = document.getElementById(ele);
        let radiobtn = document.getElementById(ele+"_radio");
        radiobtn.value = inputtext.value;
        console.log(radiobtn.value);
    }
 </script>
@endsection
