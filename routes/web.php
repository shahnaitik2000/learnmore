<?php

use App\Http\Controllers\AnswersController;
use App\Http\Controllers\AssignmentsController;
use App\Http\Controllers\FavoritesController;
use App\Http\Controllers\QuestionsController;
use App\Http\Controllers\QuizController;
use App\Http\Controllers\QuizequestionController;
use App\Http\Controllers\UsersController;
use App\Http\Controllers\VotesController;
use App\Models\User;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
})->name('home');

Route::get('/dashboard', function () {
    return view('dashboard');
})->middleware(['auth'])->name('dashboard');


Route::get('userPhoto', [UsersController::class, 'userPhoto'])->middleware(['auth'])->name('userPhoto');
Route::post('userPhoto', [UsersController::class, 'userPhotoUplaod'])->middleware(['auth'])->name('userPhoto.upload');
Route::get('notifications', [UsersController::class, 'notifications'])->middleware(['auth'])->name('notifications');
Route::post('notifyStudents/{assignment}', [AssignmentsController::class, 'notifyStudents'])->middleware(['auth', 'checkTeacher'])->name('notifystudents');

Route::resource('questions', QuestionsController::class)->except('show');

Route::get('questions/{slug}',[QuestionsController::class, 'show'])->name('questions.show');
Route::post('questions/searchById',[QuestionsController::class, 'showById'])->name('questions.showById');

Route::post('questions/{question}/favorite',[FavoritesController::class, 'store'])->name('questions.favorite');
Route::delete('questions/{question}/unfavorite',[FavoritesController::class, 'destroy'])->name('questions.unfavorite');

Route::post('question/{question}/vote/{vote}', [VotesController::class, 'voteQuestion'])->name('questions.votes');
Route::post('answers/{answer}/vote/{vote}', [VotesController::class, 'voteAnswer'])->name('answers.votes');

Route::resource('questions.answers', AnswersController::class)->except(['index','show','create']);
Route::put('answers/{answer}/best-answer',[AnswersController::class, 'bestAnswer'])->name('answers.bestAnswer');

Route::get('/assignmentsindex',function(){
    return view('assignements.assignementshome');
})->middleware(['auth', 'checkTeacher'])->name('assignment.home');

Route::get('/assignmentsindexstudent',function(){
    $teachers = User::where('type','Teacher')->get();
    return view('assignements.assignementshomestudent',compact(['teachers']));
})->middleware(['auth', 'checkStudent'])->name('assignment.homestudent');

Route::resource('assignments', AssignmentsController::class)->middleware(['auth', 'checkTeacher']);

// Route::post('quiz/teacher', [QuizController::class, ''])->middleware(['auth']);
Route::post('students/assignments',[AssignmentsController::class, 'acceptAssignment'])->middleware(['auth', 'checkStudent'])->name('students.assignment.accept');
Route::get('students/assignments',[AssignmentsController::class, 'indexStudent'])->middleware(['auth', 'checkStudent'])->name('students.assignment.index');
Route::get('students/assignments/{assignment}',[AssignmentsController::class, 'showStudent'])->middleware(['auth', 'checkStudent'])->name('students.assignment.show');
Route::get('teachers/assignments/{assignment}',[AssignmentsController::class, 'showTeacher'])->middleware(['auth', 'checkTeacher'])->name('teachers.assignment.show');
Route::post('students/assignments/{assignment}/withdraw',[AssignmentsController::class, 'withdraw'])->middleware(['auth', 'checkStudent'])->name('students.assignment.withdraw');
Route::put('students/assignments/{assignment}',[AssignmentsController::class, 'updateStudent'])->middleware(['auth', 'checkStudent'])->name('students.assignment.update');
Route::put('teachers/assignments/{assignment}',[AssignmentsController::class, 'updateTeacher'])->middleware(['auth', 'checkTeacher'])->name('teachers.assignment.update');


Route::post('quiz/student', [QuizController::class, 'studentCode'])->middleware(['auth', 'checkStudent'])->name('quiz.student');
Route::put('quiz/student', [QuizController::class, 'updateTime'])->middleware(['auth', 'checkStudent'])->name('quiz.student.update.time');
Route::get('quiz/teacher', [QuizController::class, 'showQuizHome'])->middleware(['auth', 'checkTeacher'])->name('quiz.teacher.home');
Route::post('quiz/teacher', [QuizController::class, 'store'])->middleware(['auth' ,'checkTeacher'])->name('quiz.teacher.store');
Route::get('quiz/teacher/{quiz}/question', [QuizController::class, 'questionsShow'])->middleware(['auth', 'checkTeacher'])->name('quiz.teacher.questionshow');
Route::post('quiz/teacher/{quiz}/upQuestion', [QuizController::class, 'questionUp'])->middleware(['auth', 'checkTeacher'])->name('quiz.teacher.upQuestion');
Route::post('quiz/teacher/{quiz}/downQuestion', [QuizController::class, 'questionDown'])->middleware(['auth', 'checkTeacher'])->name('quiz.teacher.downQuestion');
Route::get('quiz/teacher/{user}/manageQuizes', [QuizController::class, 'showManageQuiz'])->middleware(['auth', 'checkTeacher'])->name('quiz.teacher.manageQuiz');
Route::get('quiz/teacher/{user}/manageQuizes/{id}/edit', [QuizController::class, 'edit'])->middleware(['auth', 'checkTeacher'])->name('quiz.teacher.edit.quiz');
Route::put('quiz/teacher/{user}/manageQuizes/{id}/update', [QuizController::class, 'update'])->middleware(['auth', 'checkTeacher'])->name('quiz.teacher.update.quiz');
Route::delete('quiz/teacher/{user}/manageQuizes/{id}/delete', [QuizController::class, 'delete'])->middleware(['auth', 'checkTeacher'])->name('quiz.teacher.delete');
Route::get('quiz/attendance/{id}', [QuizController::class, 'showAttendancePage'])->middleware(['auth', 'checkStudent'])->name('quiz.showAttendancePage');
Route::post('quiz/attendance/{id}', [QuizController::class, 'markAttendance'])->middleware(['auth', 'checkStudent'])->name('quiz.markAttendance');


Route::get('quiz/teacher/addquestion', [QuizequestionController::class, 'create'])->middleware(['auth', 'checkTeacher'])->name('quiz.teacher.question.create');
Route::post('quiz/teacher/addquestion', [QuizequestionController::class, 'store'])->middleware(['auth', 'checkTeacher'])->name('quiz.teacher.question.store');
Route::get('quiz/teacher/editquestion/{quizquestion}', [QuizequestionController::class, 'edit'])->middleware(['auth', 'checkTeacher'])->name('quiz.teacher.question.edit');
Route::put('quiz/teacher/editquestion/{quizquestion}', [QuizequestionController::class, 'update'])->middleware(['auth', 'checkTeacher'])->name('quiz.teacher.question.update');
Route::delete('quiz/teacher/deletequestion/{quizquestion}', [QuizequestionController::class, 'destroy'])->middleware(['auth', 'checkTeacher'])->name('quiz.teacher.question.delete');
Route::post('quiz/teacher/re_exam', [QuizController::class, 'reexam'])->middleware(['auth', 'checkTeacher'])->name('quiz.teacher.re-exam');

Route::get('quiz/student/start/{id}', [QuizController::class, 'startQuiz'])->middleware(['auth','checkStudent'])->name('quiz.start');
Route::put('quiz/student/late', [QuizController::class, 'late'])->middleware(['auth','checkStudent'])->name('quiz.late');
Route::post('quiz/student/endQuiz', [QuizController::class, 'endQuiz'])->middleware(['auth', 'checkStudent'])->name('quiz.end');
require __DIR__.'/auth.php';
